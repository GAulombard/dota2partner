package com.hodor.dota2partner.exception;

public class TokenVerificationException extends Exception {
    public TokenVerificationException(String e) {
        super(e);
    }
}
