package com.hodor.dota2partner.exception;

public class PrivateKeyException extends Exception {
    public PrivateKeyException(String s) {
        super(s);
    }
}
