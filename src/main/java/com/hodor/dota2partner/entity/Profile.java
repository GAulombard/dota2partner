package com.hodor.dota2partner.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "profile")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "teaser_text", length = 800)
    private String teaserText;

    @Column(name = "age")
    private Integer age;

    @Column(name = "main_position")
    private Integer mainPosition;

    @Column(name = "worst_position")
    private Integer worstPosition;

    @Column(name = "looking_for_team")
    private Boolean isLookingForTeam;

    @Column(name = "looking_for_partner")
    private Boolean isLookingForPartner;

    @Column(name = "looking_for_coach")
    private Boolean isLookingForCoach;

    @Column(name = "looking_for_strategist")
    private Boolean isLookingForStrategist;

    @Column(name = "looking_for_short_caller")
    private Boolean isLookingForShortCaller;

    @Column(name = "coaching_experience")
    private Boolean hasCoachingExperience;

    @Column(name = "short_calling_aptitude")
    private Boolean hasShortCallingAptitude;

    @Column(name = "strategy_aptitude")
    private Boolean hasStrategyAptitude;

    @Column(name = "theory_craft_aptitude")
    private boolean hasTheoryCraftAptitude;

    @Column(name = "average_week_game")
    private Integer averageWeekGame;

}
