package com.hodor.dota2partner.service;

import org.springframework.stereotype.Service;

public interface MailContentService {

    String build(String message);
}
