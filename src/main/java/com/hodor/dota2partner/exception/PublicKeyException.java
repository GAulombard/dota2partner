package com.hodor.dota2partner.exception;

public class PublicKeyException extends Exception {
    public PublicKeyException(String s) {
        super(s);
    }
}
